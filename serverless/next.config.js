require('dotenv').config();
const withSourceMaps = require('@zeit/next-source-maps')();

// Use the SentryWebpack plugin to upload the source maps during build step
const SentryWebpackPlugin = require('@sentry/webpack-plugin');

const {
    SENTRY_DSN,
    SENTRY_ORG,
    SENTRY_PROJECT,
    SENTRY_AUTH_TOKEN,
    NODE_ENV,
} = process.env;

module.exports = withSourceMaps({
    env: {
        SENTRY_DSN: process.env.SENTRY_DSN,
    },
    webpack: (config, options) => {
    // When all the Sentry configuration env variables are available/configured
        // The Sentry webpack plugin gets pushed to the webpack plugins to build
        // and upload the source maps to sentry.
        // This is an alternative to manually uploading the source maps
        // Note: This is disabled in development mode.
        if (
            SENTRY_DSN
      && SENTRY_ORG
      && SENTRY_PROJECT
      && SENTRY_AUTH_TOKEN
      && NODE_ENV === 'production'
        ) {
            config.plugins.push(
                new SentryWebpackPlugin({
                    release: options.buildId,
                    include: '.next',
                    ignore: ['node_modules'],
                    urlPrefix: '~/_next',
                }),
            );
        }

        return config;
    },
});
