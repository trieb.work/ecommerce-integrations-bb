import { Invoice } from '@trieb.work/zoho-inventory-ts';
import { GetOrderQuery, GetOrderQueryVariables, OrderCaptureMutation, OrderCaptureMutationVariables } from 'gqlTypes/globalTypes';
import SaleorClient from 'lib/ApolloSetup';
import { getOrderQuery, orderCaptureMutation } from 'lib/gql/saleor.gql';
import { getGqlOrderId } from '@trieb.work/helpers-ts';
import winston from 'winston';

const client = SaleorClient();

const captureOrderSaleor = async (invoice :Invoice, logger :winston.Logger) => {
    const referenceNumber = invoice?.reference_number;
    if (!referenceNumber?.match(/STORE-\d+/)) return true;
    const orderNumber = referenceNumber.split('-')[1];
    const saleorOrderId = getGqlOrderId(orderNumber);
    const saleorOrder = await client.query<GetOrderQuery, GetOrderQueryVariables>({
        query: getOrderQuery,
        variables: {
            id: saleorOrderId,
        },
    });
    if (!saleorOrder.data?.order?.id) return true;
    if (saleorOrder.data.order.paymentStatus !== 'NOT_CHARGED') {
        logger.info('Order has not status "NOT_CHARGED" in Saleor - can not capture payment');
        return true;
    }
    const { order } = saleorOrder.data;
    if (invoice.total !== order.total.gross.amount) {
        logger.info('Can not automatically capture payment. Invoice total and order total diverge. This is most likely an error');
        return true;
    }
    await client.mutate<OrderCaptureMutation, OrderCaptureMutationVariables>({
        mutation: orderCaptureMutation,
        variables: {
            id: saleorOrderId,
            amount: invoice.total,
        },

    });
    return true;
};
export default captureOrderSaleor;
