type ECommerceCustomer = {
    id :string,
    email_address? :string;
    first_name?: string;
    last_name?: string;
    opt_in_status?: boolean;
    company?: string;
};

type MailchimpMemberStatus = 'subscribed' | 'unsubscribed' | 'cleaned' | 'pending' | 'transactional';
type Orderline = {
    id:string,
    product_id:string,
    product_variant_id:string,
    quantity:number,
    price:number,
    discount:number,
    title:string,
};
type Promos = { code: string, amount_discounted: number, type: 'fixed' };

type MailchimpOrder = {
    id: string,
    customer: {},
    currency_code: string,
    order_total: number|string,
    shipping_total: number|string,
    tax_total:number|string,
    processed_at_foreign: string,
    lines: Orderline[],
    shipping_address: {
        name:string,
        address1: string,
        address2: string,
        city: string,
        postal_code: string,
        country?: string,
        country_code?: string,
        company:string,
    },
    billing_address: {
        name:string,
        address1: string,
        address2: string,
        city: string,
        postal_code: string,
        country?: string,
        country_code?: string,
        company: string,
    },
    discount_total: number,
    promos: Promos[],
    landing_site: string,
    financial_status: 'pending',
    fulfillment_status: 'pending',
    order_url: string,

};
