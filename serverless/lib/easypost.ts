import EasyPost from '@easypost/api';

type TrackingStatus = 'delivered' | 'in_transit' | 'out_for_delivery';
export type Tracker = {
    'id': string,
    'object': 'Event',
    'created_at': '2014-11-19T10:51:54Z',
    'updated_at': '2014-11-19T10:51:54Z',
    'description': 'tracker.updated',
    'mode': 'test',
    'previous_attributes': {
        'status': 'unknown'
    },
    'pending_urls': [],
    'completed_urls': [],
    'result': {
        'id': 'trk_Txyy1vaM',
        'object': 'Tracker',
        'mode': 'test',
        'tracking_code': string,
        'status': TrackingStatus,
        'created_at': '2014-11-18T10:51:54Z',
        'updated_at': '2014-11-18T10:51:54Z',
        'signed_by': 'John Tester',
        'weight': 17.6,
        'est_delivery_date': '2014-08-27T00:00:00Z',
        'shipment_id': null,
        'carrier': 'UPS',
        'public_url': 'https://track.easypost.com/djE7...',
        'tracking_details': [
            {
                'object': 'TrackingDetail',
                'message': 'BILLING INFORMATION RECEIVED',
                'status': 'pre_transit',
                'datetime': '2014-08-21T14:24:00Z',
                'tracking_location': {
                    'object': 'TrackingLocation',
                    'city': null,
                    'state': null,
                    'country': null,
                    'zip': null
                }
            }, {
                'object': 'TrackingDetail',
                'message': 'ORIGIN SCAN',
                'status': 'in_transit',
                'datetime': '2014-08-21T14:48:00Z',
                'tracking_location': {
                    'object': 'TrackingLocation',
                    'city': 'SOUTH SAN FRANCISCO',
                    'state': 'CA',
                    'country': 'US',
                    'zip': null
                }
            }, {
                'object': 'TrackingDetail',
                'message': 'DEPARTURE SCAN',
                'status': 'in_transit',
                'datetime': '2014-08-22T08:51:00Z',
                'tracking_location': {
                    'object': 'TrackingLocation',
                    'city': 'SOUTH SAN FRANCISCO',
                    'state': 'CA',
                    'country': 'US',
                    'zip': null
                }
            }, {
                'object': 'TrackingDetail',
                'message': 'ARRIVAL SCAN',
                'status': 'in_transit',
                'datetime': '2014-08-23T09:31:00Z',
                'tracking_location': {
                    'object': 'TrackingLocation',
                    'city': 'SAN FRANCISCO',
                    'state': 'CA',
                    'country': 'US',
                    'zip': null
                }
            }, {
                'object': 'TrackingDetail',
                'message': 'OUT FOR DELIVERY',
                'status': 'out_for_delivery',
                'datetime': '2014-08-24T08:10:00Z',
                'tracking_location': {
                    'object': 'TrackingLocation',
                    'city': 'SAN FRANCISCO',
                    'state': 'CA',
                    'country': 'US',
                    'zip': null
                }
            }, {
                'object': 'TrackingDetail',
                'message': 'DELIVERED',
                'status': 'delivered',
                'datetime': '2014-08-24T15:33:00Z',
                'tracking_location': {
                    'object': 'TrackingLocation',
                    'city': 'SAN FRANCISCO',
                    'state': 'CA',
                    'country': 'US',
                    'zip': null
                }
            },
        ]
    }
};

const init = () => {
    const easyPostApiKey = process.env.EASYPOST_KEY;
    if (!easyPostApiKey) {
        console.error('Easypost API Key not set! Use env variable EASYPOST_KEY');
        return false;
    }
    const easypost = new EasyPost(easyPostApiKey);
    return easypost;
};
type Carrier = 'DPD';
export const addTracker = async ({ trackingCode, carrier }:{trackingCode :string, carrier: Carrier }) => {
    const easypost = init();
    if (!easypost) return false;
    const tracker = new easypost.Tracker({
        tracking_code: trackingCode,
        carrier,
    });
    await tracker.save();

    return true;
};
