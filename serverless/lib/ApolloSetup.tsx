import { ApolloClient, InMemoryCache, ApolloLink, HttpLink } from '@apollo/client';
import { RetryLink } from '@apollo/client/link/retry';
import { onError } from '@apollo/client/link/error';

const loggerLink = new ApolloLink((operation, forward) => {
    console.log(`GraphQL Request: ${operation.operationName}`);
    operation.setContext({ start: new Date() });
    return forward(operation).map((response) => {
        const responseTime = new Date().getTime() - new Date(operation.getContext().start).getTime();
        console.log(`GraphQL Response took: ${responseTime}`);
        return response;
    });
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
        graphQLErrors.map(({ message }) => console.log(`GraphQL Error: ${message}`));
    }
    if (networkError) {
        console.log(`Network Error: ${networkError.message}`);
    }
});

/**
 * Wrapper around the GraphQL Apollo library used for communication with the Saleor API
 * @param authenticated Call the GraphQL API as App user (standard) or disable authentication
 */
export default function Client(authenticated = true) {
    const token = authenticated ? process.env.SALEOR_AUTH_TOKEN : null;

    const links = ApolloLink.from([
        loggerLink,
        new RetryLink({
            attempts: {
                max: 3,
            } }),
        errorLink,
        new HttpLink({ uri: `${process.env.SALEOR_URL}/graphql/`,
            headers: {
                authorization: token ? `Bearer ${token}` : '',
            } }),
    ]);

    const client = new ApolloClient({
        link: links,
        headers: {
            authorization: token ? `Bearer ${token}` : '',
        },
        cache: new InMemoryCache(),
    });

    return client;
}
